package com.example.roomsqlitecontentprovider.models.contacts

import androidx.lifecycle.LiveData
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity
import kotlinx.coroutines.flow.Flow

interface ContactsRepository {
    val readAllData: LiveData<List<ContactDbEntity>>
    suspend fun addContact(contactData: Contact)
    suspend fun updateContact(contactData: Contact)
    suspend fun deleteAllContacts()
    suspend fun deleteContact(contact: Contact)
}