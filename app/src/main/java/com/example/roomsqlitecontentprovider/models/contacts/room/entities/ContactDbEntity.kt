package com.example.roomsqlitecontentprovider.models.contacts.room.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(
    tableName = "contacts",
)
data class ContactDbEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String,
    @ColumnInfo(name = "phone_number") val phoneNumber: String,
    var note: String,
): Parcelable {
    companion object {
        fun fromContactData(contactData: Contact) = ContactDbEntity(
            id = 0, // SQLite generates identifier automatically if ID = 0
            name = contactData.name,
            phoneNumber = contactData.phoneNumber,
            note = contactData.note,
        )
    }
}