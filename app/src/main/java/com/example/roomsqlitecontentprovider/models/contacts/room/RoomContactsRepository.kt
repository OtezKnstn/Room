package com.example.roomsqlitecontentprovider.models.contacts.room

import androidx.lifecycle.LiveData
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class RoomContactsRepository(
    private val contactDao: ContactDao,
) {
    val readAllData: LiveData<List<ContactDbEntity>> = contactDao.readAllData()

    suspend fun addContact(contactData: Contact) {
        val entity = ContactDbEntity.fromContactData(contactData)
        contactDao.addContact(entity)
    }

    suspend fun updateContact(contactData: Contact) {
        val entity = ContactDbEntity.fromContactData(contactData)
        contactDao.updateContact(entity)
    }

    suspend fun deleteAllContacts(){
        contactDao.deleteAllContacts()
    }

    suspend fun deleteContact(contact: Contact){
        val entity = ContactDbEntity.fromContactData(contact)
        contactDao.deleteContact(entity)
    }
}