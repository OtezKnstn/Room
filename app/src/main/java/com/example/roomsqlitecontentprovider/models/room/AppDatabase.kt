package com.example.roomsqlitecontentprovider.models.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.roomsqlitecontentprovider.models.contacts.room.ContactDao
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity

@Database(
    version = 1,
    entities = [
        ContactDbEntity::class,
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getNotesDao(): ContactDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }

            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "contacts"
                )
//                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}