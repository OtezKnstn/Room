package com.example.roomsqlitecontentprovider.models.contacts.entities

data class Contact(
    val id: Long,
    val name: String,
    val phoneNumber: String,
    val note: String
)
