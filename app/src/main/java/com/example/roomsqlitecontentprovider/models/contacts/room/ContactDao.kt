package com.example.roomsqlitecontentprovider.models.contacts.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE, entity = ContactDbEntity::class)
    suspend fun addContact(contactDbEntity: ContactDbEntity)

    @Update
    suspend fun updateContact(contact: ContactDbEntity)

    @Query("DELETE FROM contacts")
    suspend fun deleteAllContacts()

    @Delete
    suspend fun deleteContact(contact: ContactDbEntity)

    @Query("SELECT * FROM contacts ORDER BY id ASC")
    fun readAllData(): LiveData<List<ContactDbEntity>>
}
