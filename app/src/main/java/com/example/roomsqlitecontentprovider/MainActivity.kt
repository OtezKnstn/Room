package com.example.roomsqlitecontentprovider

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact
import com.example.roomsqlitecontentprovider.ui.contact.ContactViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var contactViewModel: ContactViewModel
    private var checkPos: Boolean = true
    private lateinit var sharedPref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        contactViewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        sharedPref = getSharedPreferences("isCan", MODE_PRIVATE)
        checkPos = sharedPref.getBoolean("ifCan", true)
        if(checkPos)
            getContacts()
    }
    @SuppressLint("Range", "CommitPrefEdits", "Recycle")
    fun getContacts(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 0)
        }
        val contentResolver: ContentResolver = contentResolver
        val uri: Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val cursor: Cursor? = contentResolver.query(uri, null, null, null, null)
        Log.d("Nothing", "${cursor?.count}")
        if (cursor?.count!! > 0) {
            while(cursor.moveToNext()) {
                val name: String = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                val phone: String = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                insertDataToDatabase(name, phone)
            }
        }

        if(checkPos) {
            checkPos = false
            sharedPref.edit().putBoolean("ifCan", checkPos)
        }
    }

    private fun insertDataToDatabase(string: String, number: String){
        val contact = Contact(0, string, number, "")
        contactViewModel.addContact(contact)
    }

//    fun deleteCurrentTable() {
//        contactViewModel.deleteAllContacts()
//    }
}