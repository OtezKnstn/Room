package com.example.roomsqlitecontentprovider.ui.main


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.roomsqlitecontentprovider.R
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity


class ListAdapter: RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var contactList = emptyList<ContactDbEntity>()
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val name: TextView = itemView.findViewById(R.id.NameOnCard)
        val phone: TextView = itemView.findViewById(R.id.PhoneOnCard)
        val text: TextView = itemView.findViewById(R.id.NotesOnCard)
        val card: LinearLayout = itemView.findViewById(R.id.contactCard)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.contact_card, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = contactList[position]
        holder.name.text = currentItem.name
        holder.phone.text = currentItem.phoneNumber
        holder.text.text = currentItem.note

        holder.card.setOnClickListener {
            val action = MainFragmentDirections.toInfo(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(contact: List<ContactDbEntity>){
        this.contactList = contact
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return contactList.size
    }
}