package com.example.roomsqlitecontentprovider.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomsqlitecontentprovider.databinding.FragmentMainBinding
import com.example.roomsqlitecontentprovider.ui.contact.ContactViewModel


class MainFragment : Fragment() {
    private lateinit var contactViewModel: ContactViewModel
    private lateinit var adapter: ListAdapter
    private lateinit var binding: FragmentMainBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ListAdapter()
        val recyclerView = binding.recycleView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        contactViewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        contactViewModel.readAllData.observe(viewLifecycleOwner) { contact ->
            adapter.setData(contact)
            Log.d("vales", adapter.itemCount.toString())
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = MainFragment()
    }
}