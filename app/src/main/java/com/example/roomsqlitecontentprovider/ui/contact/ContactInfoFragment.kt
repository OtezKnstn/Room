package com.example.roomsqlitecontentprovider.ui.contact

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.roomsqlitecontentprovider.R
import com.example.roomsqlitecontentprovider.databinding.FragmentContactInfoBinding
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact

class ContactInfoFragment : Fragment() {
    private lateinit var binding: FragmentContactInfoBinding
    private lateinit var contactInfo: ContactViewModel

    private val args by navArgs<ContactInfoFragmentArgs>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentContactInfoBinding.inflate(inflater)

        contactInfo = ViewModelProvider(this)[ContactViewModel::class.java]

        binding.nameOnInfo.text = args.currentContact.name
        binding.phoneOnInfo.text = args.currentContact.phoneNumber
        binding.noteOnInfo.text = args.currentContact.id.toString()
        binding.notesOnInfo.setText(args.currentContact.note, TextView.BufferType.EDITABLE)

        binding.addNote.setOnClickListener {
            changeNote()
            Toast.makeText(requireContext(), getString(R.string.note_changed), Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.toMain)
        }

        return binding.root
    }

    private fun changeNote(){
        val note = binding.notesOnInfo.text.toString()
        val contact = Contact(args.currentContact.id, args.currentContact.name, args.currentContact.phoneNumber, note)
        contactInfo.updateContact(contact)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactInfoFragment()
    }
}