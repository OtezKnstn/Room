package com.example.roomsqlitecontentprovider.ui.contact

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.roomsqlitecontentprovider.models.contacts.entities.Contact
import com.example.roomsqlitecontentprovider.models.contacts.room.RoomContactsRepository
import com.example.roomsqlitecontentprovider.models.contacts.room.entities.ContactDbEntity
import com.example.roomsqlitecontentprovider.models.room.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactViewModel(application: Application): AndroidViewModel(application) {
    private var repository: RoomContactsRepository
    var readAllData: LiveData<List<ContactDbEntity>>

    init {
        val contactDao = AppDatabase.getDatabase(application).getNotesDao()
        repository = RoomContactsRepository(contactDao)
        readAllData = repository.readAllData
    }

    fun updateContact(contact: Contact){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateContact(contact)
        }
    }

    fun addContact(contact: Contact){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addContact(contact)
        }
    }

    fun deleteContact(contact: Contact){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteContact(contact)
        }
    }

    fun deleteAllContacts(){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllContacts()
        }
    }
}
